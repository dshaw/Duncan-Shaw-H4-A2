1.)

At first I was having trouble understading the program, even after running it.
I decided, in response, to try adding a cout for a and b within the function to get a better understanding of what's happening.
just before summing, "a" was 9 and "b" was 54. From this my results make a lot more sense.
What is happening is v1's address is changed to a's address.
In this case, it's value doesn't change. 
Then 5 is added to it's value, raising it to 9, which is what it ends up as.
Next, b, which for the function was v2 points to a.
Another way of phrasing this is that the value at v2 is changed to the value pointed to by v1. 
In this case, the value of v2 becomes the value at the thing v1 points to. 
That just so happened to be 54 for b. which is it's final value.
z becomes (v1+v2/)2-7
After pluggin in numbers you get (61)/2-7
since z is an int 61/2 rounds to 31.
31-7=24 which is z's final value.


2.)

My predictions were that numbers= [1,4,1,4,(some address),5]

My logic was that it starts by pointing p at numbers. 
Thus p will point to numbers[0]
it then changes that value to 1
next p moves to the next address, numbers[1]
it changes that value to 4
next it points the numbers[2]
it changes the value there to the value at numbers[0] which is 1
Here's where I messed up.
My logic was that p should point to numbers+ *p meant the current thing it points to plus the value stored there.
The correct logic is that p should point to numbers[0] plus the number stored at what p is currently pointing to 
in this case p is still pointing to numbers[2] and the value stored there is 1.
Thus, p should point to numbers[1] and change the value to 4, which is the value it already had.
for the next part p would point to numbers +1=numbers[1]
it would then change the value 4 locations down to 5. in this case it changes numbers[5] to 5. 
since numbers[4] and numbers [3] didn't weren't assigned values, they would just be an address.
Here is my printout:
"Numbers : 1 4 1 134538437 134538288 5"

