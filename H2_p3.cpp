#include <iostream>
#include <math.h>
using namespace std;


//my structure to a define a four-momentum vector
struct fourmom
{
	float mom [3];
	float E;
};


//my function to take a four-momentum vector and find it's invariant mass
//I use E_0 ^ 2 = E ^ 2 - ||p|| ^ 2
float mass(fourmom particle)
{
	double m;

	m= particle.mom[0] * particle.mom[0] + particle.mom[1] * particle.mom[1] + particle.mom[2] * particle.mom[2];
	m=pow(particle.E,2)-m;
	m=sqrt(m);
	cout<<"the invariant mass is "<<m<<endl;
return 0;
}


//my function to find the invariant mass of two particles
//I use E_0^ 2 = ( E1 + E2 ) ^ 2 - || p1 - p2 || ^ 2
float twopmass(fourmom particlea,fourmom particleb)
{
	double ma;
	ma= pow(particlea.mom[0]+particleb.mom[0],2.f)+pow(particlea.mom[1]+particleb.mom[1],2.f)+pow(particlea.mom[2]+particleb.mom[2],2.f);
	ma=pow(particlea.E+particleb.E,2)-ma;
	ma=sqrt(ma);
	cout<<"the invariant mass of the system is "<<ma<<endl;
return 0;
}


int main ()
{
	fourmom particle1;
	fourmom particle2;
	
	//my values for particle 2
	particle2.E= 10.5*pow(10.,6.);
	particle2.mom [0]= 7 * pow(10.,6.);
	particle2.mom [1]= 3 * pow(10.,6.);
	particle2.mom [2]= 7 * pow(10.,6.);

	//my values for particle 1
	particle1.E= 6*pow(10.,6.);
	particle1.mom [0]= 2 * pow(10.,6.);
	particle1.mom [1]= 2 * pow(10.,6.);
	particle1.mom [2]= 5 * pow(10.,6.);
	
	//here is where I execute all three functions
	mass(particle1);
	mass(particle2);
	twopmass(particle1,particle2);
	return 0;
}
